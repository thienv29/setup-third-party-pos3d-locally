#!/bin/bash

kong_host="http://localhost:8001"
counter=0
while true; do
    printf "Deleting %s%%\r" "$counter"
    ((counter+=10))

    consumer_ids=$(curl -s $kong_host/consumers | jq -r '.data[].id')
    route_ids=$(curl -s $kong_host/routes | jq -r '.data[].id')
    service_ids=$(curl -s $kong_host/services | jq -r '.data[].id')
    upstream_ids=$(curl -s $kong_host/upstreams | jq -r '.data[].id')
    plugin_ids=$(curl -s $kong_host/plugins | jq -r '.data[].id')
    
    # Check the condition for continuation
    if [[ -z $consumer_ids && -z $route_ids && -z $service_ids && -z $upstream_ids && -z $plugin_ids ]]; then
        break
    fi
    
    # Delete each consumer
    for consumer_id in $consumer_ids; do
        curl -f -s -X DELETE "$kong_host/consumers/$consumer_id"
    done
    
    # Delete each route
    for route_id in $route_ids; do
        curl -f -s -X DELETE "$kong_host/routes/$route_id"
    done
    
    # Delete each service
    for service_id in $service_ids; do
        curl -f -s -X DELETE "$kong_host/services/$service_id"
    done
    
    # Delete each upstream
    for upstream_id in $upstream_ids; do
        curl -f -s -X DELETE "$kong_host/upstreams/$upstream_id"
    done
    
    # Delete each plugin
    for plugin_id in $plugin_ids; do
        curl -f -s -X DELETE "$kong_host/plugins/$plugin_id"
    done
    
done

# Lệnh tiếp theo sau khi thoát khỏi vòng lặp
echo "Reset kong completed"
