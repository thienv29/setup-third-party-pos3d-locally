#!/bin/bash

# Mặc định giá trị cho địa chỉ IP
ip_address="127.0.0.1"

# Xử lý các tùy chọn dòng lệnh
while getopts ":i:" opt; do
    case $opt in
        i)
            ip_address=$OPTARG
        ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
        ;;
    esac
done

# Bỏ qua các tùy chọn đã xử lý
shift $((OPTIND -1))

kong_domain="localhost:8000"
elastic_domain="$ip_address:5556"
user_domain="$ip_address:5557"
order_domain="$ip_address:5558"
product_domain="$ip_address:5555"
kong_domain_private="localhost:8001"
proto="http"
# auth jwt
consumer_name="ADMIN_SOFT"
secret_key="pos3d@321"
key_id="vKy4R2HEYhpHwkU7q0wqsTcTGlWZKPYu"

# Tạo upstream cho Elasticsearch
curl -s -X POST $proto://$kong_domain_private/upstreams \
-H "Content-Type: application/json" \
-d '{
    "name": "elasticsearch"
}'

# Cấu hình service Elasticsearch
curl -X POST $proto://$kong_domain_private/services \
-H "Content-Type: application/json" \
-d '{
    "name": "elasticsearch",
    "url": "'$proto://elasticsearch'",
    "host": "elasticsearch",
    "port": "'$port_target'"
}'

# Thêm target cho Elasticsearch upstream
curl -s -X POST $proto://$kong_domain_private/upstreams/elasticsearch/targets \
-H "Content-Type: application/json" \
-d '{
    "target": "'$elastic_domain'"
}'

# Cấu hình route cho Elasticsearch
curl -s -X POST $proto://$kong_domain_private/services/elasticsearch/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/elasticsearch"],
    "strip_path": true,
    "name": "elasticsearch",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

# Tạo upstream cho Product
curl -s -X POST $proto://$kong_domain_private/upstreams \
-H "Content-Type: application/json" \
-d '{
    "name": "product"
}'

# Cấu hình service Product
curl -X POST $proto://$kong_domain_private/services \
-H "Content-Type: application/json" \
-d '{
    "name": "product",
    "url": "'$proto://product'",
    "host": "product",
    "port": "'$port_target'"
}'


# Thêm target cho Product upstream
curl -s -X POST $proto://$kong_domain_private/upstreams/product/targets \
-H "Content-Type: application/json" \
-d '{
    "target": "'$product_domain'"
}'

# Cấu hình route cho Product
curl -s -X POST $proto://$kong_domain_private/services/product/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/product"],
    "strip_path": true,
    "name": "product",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

# Tạo upstream cho User
curl -s -X POST $proto://$kong_domain_private/upstreams \
-H "Content-Type: application/json" \
-d '{
    "name": "user"
}'

# Cấu hình service User
curl -X POST $proto://$kong_domain_private/services \
-H "Content-Type: application/json" \
-d '{
    "name": "user",
    "url": "'$proto://user'",
    "host": "user",
    "port": "'$port_target'"
}'

# Thêm target cho User upstream
curl -s -X POST $proto://$kong_domain_private/upstreams/user/targets \
-H "Content-Type: application/json" \
-d '{
    "target": "'$user_domain'"
}'

# Cấu hình route cho User
curl -s -X POST $proto://$kong_domain_private/services/user/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/user"],
    "strip_path": true,
    "name": "user",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'


# Tạo upstream cho order
curl -s -X POST $proto://$kong_domain_private/upstreams \
-H "Content-Type: application/json" \
-d '{
    "name": "order"
}'

# Cấu hình service order
curl -X POST $proto://$kong_domain_private/services \
-H "Content-Type: application/json" \
-d '{
    "name": "order",
    "url": "'$proto://order'",
    "host": "order",
    "port": "'$port_target'"
}'

# Thêm target cho order upstream
curl -s -X POST $proto://$kong_domain_private/upstreams/order/targets \
-H "Content-Type: application/json" \
-d '{
    "target": "'$order_domain'"
}'

# Cấu hình route cho order
curl -s -X POST $proto://$kong_domain_private/services/order/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/order"],
    "strip_path": true,
    "name": "order",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

# Cấu hình route cho anony
curl -s -X POST $proto://$kong_domain_private/services/user/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/user/anonymous", "/user/swagger"],
    "strip_path": true,
    "name": "user-anonymous",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'


curl -s -X POST $proto://$kong_domain_private/services/product/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/product/anonymous","/product/swagger"],
    "strip_path": true,
    "name": "product-anonymous",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

curl -s -X POST $proto://$kong_domain_private/services/elasticsearch/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/elasticsearch/anonymous","/elasticsearch/swagger"],
    "strip_path": true,
    "name": "elasticsearch-anonymous",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

curl -s -X POST $proto://$kong_domain_private/services/order/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/order/anonymous","/order/swagger"],
    "strip_path": true,
    "name": "order-anonymous",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

curl -X POST $proto://$kong_domain_private/consumers \
-H "Content-Type: application/json" \
-d '{
    "username": "'$consumer_name'",
    "custom_id": "9dda4131-d10b-4081-9397-f7b964cf69b6"
}'


curl -X POST $proto://$kong_domain_private/consumers/$consumer_name/jwt \
-H "Content-Type: application/json" \
-d '{
    "algorithm": "HS256",
    "key": "'$key_id'",
    "secret": "'$secret_key'"
}'

curl -X POST $proto://$kong_domain_private/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "USER_MANAGEMENT"
}'

curl -X POST $proto://$kong_domain_private/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "PRODUCT_MANAGEMENT"
}'

curl -X POST $proto://$kong_domain_private/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "SCENE_MANAGEMENT"
}'

curl -X POST $proto://$kong_domain_private/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "ELASTIC_CONTROL"
}'

curl -X POST $proto://$kong_domain_private/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "ORDER_MANAGEMENT"
}'

# Cấu hình JWT
curl -X POST $proto://$kong_domain_private/routes/product/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "jwt",
    "config": {
      "run_on_preflight": true,
      "header_names": ["Authorization"],
      "key_claim_name": "kid",
      "secret_is_base64": false,
      "uri_param_names": ["jwt"]
    }
}'


curl -X POST $proto://$kong_domain_private/routes/elasticsearch/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "jwt",
    "config": {
      "run_on_preflight": true,
      "header_names": ["Authorization"],
      "key_claim_name": "kid",
      "secret_is_base64": false,
      "uri_param_names": ["jwt"]
    }
}'


curl -X POST $proto://$kong_domain_private/routes/user/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "jwt",
    "config": {
      "run_on_preflight": true,
      "header_names": ["Authorization"],
      "key_claim_name": "kid",
      "secret_is_base64": false,
      "uri_param_names": ["jwt"]
    }
}'

curl -X POST $proto://$kong_domain_private/routes/order/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "jwt",
    "config": {
      "run_on_preflight": true,
      "header_names": ["Authorization"],
      "key_claim_name": "kid",
      "secret_is_base64": false,
      "uri_param_names": ["jwt"]
    }
}'

# Cấu hình CORS
curl -X POST $proto://$kong_domain_private/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "cors",
    "config": {
      "origins": ["*"],
      "methods": ["GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"],
      "credentials": true
    }
}'


# Cấu hình ACL
curl -X POST $proto://$kong_domain_private/routes/product/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "acl",
    "config": {
      "allow": ["PRODUCT_MANAGEMENT","SCENE_MANAGEMENT"]
    }
}'


curl -X POST $proto://$kong_domain_private/routes/elasticsearch/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "acl",
    "config": {
      "allow": ["ELASTIC_CONTROL"]
    }
}'


curl -X POST $proto://$kong_domain_private/routes/user/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "acl",
    "config": {
      "allow": ["USER_MANAGEMENT"]
    }
}'


curl -X POST $proto://$kong_domain_private/routes/order/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "acl",
    "config": {
      "allow": ["ORDER_MANAGEMENT"]
    }
}'
