# $ ./config.sh
#!/bin/bash

# Dommain của các services.
elastic_domain="dev-elastic.pos3d.vn"
user_domain="dev-user.pos3d.vn"
product_domain="dev-product.pos3d.vn"
order_domain="dev-order.pos3d.vn"

# Domain và protocol gọi kong admin
kong_admin_domain="kong-admin.pos3d.vn:443"
proto="https"

# protocol và port của các services
proto_target="https"
port_target="443"

# auth jwt
consumer_name="ADMIN_SOFT"
secret_key="pos3d@321"
key_id="vKy4R2HEYhpHwkU7q0wqsTcTGlWZKPYu"

# Tạo upstream cho Elasticsearch
curl -X POST $proto://$kong_admin_domain/upstreams \
-H "Content-Type: application/json" \
-d '{
      "name": "elasticsearch"
}'

# Cấu hình service Elasticsearch
curl -X POST $proto://$kong_admin_domain/services \
-H "Content-Type: application/json" \
-d '{
    "name": "elasticsearch",
    "url": "'$proto_target://elasticsearch'",
    "host": "elasticsearch",
    "port": "'$port_target'"
}'

# Thêm target cho Elasticsearch upstream
curl -X POST $proto://$kong_admin_domain/upstreams/elasticsearch/targets \
-H "Content-Type: application/json" \
-d '{
    "target": "'$elastic_domain:$port_target'"
}'

# Cấu hình route cho Elasticsearch
curl -X POST $proto://$kong_admin_domain/services/elasticsearch/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/elasticsearch"],
    "strip_path": true,
    "name": "elasticsearch",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

# Tạo upstream cho Product
curl -X POST $proto://$kong_admin_domain/upstreams \
-H "Content-Type: application/json" \
-d '{
    "name": "product"
}'

# Cấu hình service Product
curl -X POST $proto://$kong_admin_domain/services \
-H "Content-Type: application/json" \
-d '{
    "name": "product",
    "url": "'$proto_target://product'",
    "host": "product",
    "port": "'$port_target'"
}'

# Thêm target cho Product upstream
curl -X POST $proto://$kong_admin_domain/upstreams/product/targets \
-H "Content-Type: application/json" \
-d '{
    "target": "'$product_domain:$port_target'"
}'

# Cấu hình route cho Product
curl -X POST $proto://$kong_admin_domain/services/product/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/product"],
    "strip_path": true,
    "name": "product",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

# Tạo upstream cho User
curl -X POST $proto://$kong_admin_domain/upstreams \
-H "Content-Type: application/json" \
-d '{
    "name": "user"
}'

# Cấu hình service User
curl -X POST $proto://$kong_admin_domain/services \
-H "Content-Type: application/json" \
-d '{
    "name": "user",
    "url": "'$proto_target://user'",
    "host": "user",
    "port": "'$port_target'"
}'

# Thêm target cho User upstream
curl -X POST $proto://$kong_admin_domain/upstreams/user/targets \
-H "Content-Type: application/json" \
-d '{
    "target": "'$user_domain:$port_target'"
}'

# Cấu hình route cho User
curl -X POST $proto://$kong_admin_domain/services/user/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/user"],
    "strip_path": true,
    "name": "user",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

# Tạo upstream cho Order
curl -X POST $proto://$kong_admin_domain/upstreams \
-H "Content-Type: application/json" \
-d '{
    "name": "order"
}'
# Thêm target cho Order upstream
curl -X POST $proto://$kong_admin_domain/upstreams/order/targets \
-H "Content-Type: application/json" \
-d '{
    "target": "'$order_domain:$port_target'"
}'

# Cấu hình service Order
curl -X POST $proto://$kong_admin_domain/services \
-H "Content-Type: application/json" \
-d '{
    "name": "order",
    "url": "'$proto_target://order'",
    "host": "order",
    "port": "'$port_target'"
}'

# Cấu hình route cho Order
curl -X POST $proto://$kong_admin_domain/services/order/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/order"],
    "strip_path": true,
    "name": "order",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'


# Cấu hình route cho anony
curl -s -X POST $proto://$kong_admin_domain/services/user/routes \
-H "Content-Type: application/json" \
-d '{
    "paths": ["/user/anonymous"],
    "strip_path": true,
    "name": "user-anonymous",
    "methods": ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
}'

curl -X POST $proto://$kong_admin_domain/consumers \
-H "Content-Type: application/json" \
-d '{
    "username": "'$consumer_name'",
    "custom_id": "9dda4131-d10b-4081-9397-f7b964cf69b6"
}'


curl -X POST $proto://$kong_admin_domain/consumers/$consumer_name/jwt \
-H "Content-Type: application/json" \
-d '{
    "algorithm": "HS256",
    "key": "'$key_id'",
    "secret": "'$secret_key'"
}'

curl -X POST $proto://$kong_admin_domain/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "USER_MANAGEMENT"
}'

curl -X POST $proto://$kong_admin_domain/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "PRODUCT_MANAGEMENT"
}'

curl -X POST $proto://$kong_admin_domain/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "SCENE_MANAGEMENT"
}'

curl -X POST $proto://$kong_admin_domain/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "ELASTIC_CONTROL"
}'

curl -X POST $proto://$kong_admin_domain/consumers/$consumer_name/acls \
-H "Content-Type: application/json" \
-d '{
    "group": "ORDER_MANAGEMENT"
}'

# Cấu hình JWT
curl -X POST $proto://$kong_admin_domain/routes/product/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "jwt",
    "config": {
      "run_on_preflight": true,
      "header_names": ["Authorization"],
      "key_claim_name": "kid",
      "secret_is_base64": false,
      "uri_param_names": ["jwt"]
    }
}'


curl -X POST $proto://$kong_admin_domain/routes/elasticsearch/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "jwt",
    "config": {
      "run_on_preflight": true,
      "header_names": ["Authorization"],
      "key_claim_name": "kid",
      "secret_is_base64": false,
      "uri_param_names": ["jwt"]
    }
}'


curl -X POST $proto://$kong_admin_domain/routes/user/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "jwt",
    "config": {
      "run_on_preflight": true,
      "header_names": ["Authorization"],
      "key_claim_name": "kid",
      "secret_is_base64": false,
      "uri_param_names": ["jwt"]
    }
}'


curl -X POST $proto://$kong_admin_domain/routes/order/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "jwt",
    "config": {
      "run_on_preflight": true,
      "header_names": ["Authorization"],
      "key_claim_name": "kid",
      "secret_is_base64": false,
      "uri_param_names": ["jwt"]
    }
}'

# Cấu hình CORS
curl -X POST $proto://$kong_admin_domain/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "cors",
    "config": {
      "origins": ["*"],
      "methods": ["GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"],
      "credentials": true
    }
}'


# Cấu hình ACL
curl -X POST $proto://$kong_admin_domain/routes/product/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "acl",
    "config": {
      "allow": ["PRODUCT_MANAGEMENT","SCENE_MANAGEMENT"]
    }
}'


curl -X POST $proto://$kong_admin_domain/routes/elasticsearch/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "acl",
    "config": {
      "allow": ["ELASTIC_CONTROL"]
    }
}'


curl -X POST $proto://$kong_admin_domain/routes/user/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "acl",
    "config": {
      "allow": ["USER_MANAGEMENT"]
    }
}'


curl -X POST $proto://$kong_admin_domain/routes/order/plugins \
-H "Content-Type: application/json" \
-d '{
    "name": "acl",
    "config": {
      "allow": ["ORDER_MANAGEMENT"]
    }
}'
