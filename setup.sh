# Step 1: ipconfig
# Step 2: ./setup.sh -i <your_ipaddress>
#!/bin/bash

function cleanup {
    echo "Cleaning up..."
    docker-compose down
    docker network rm kong-net
    docker network rm es-net
    echo "Cleanup complete."
    exit 1
}

function setup_elasticsearch {
    echo "Setting up Elasticsearch..."
    cd "docker-compose/elasticsearch/"
    
    # Check if the es-net network exists
    if ! docker network inspect es-net >/dev/null 2>&1; then
        docker network create es-net
    else
        echo "es-net network already exists."
    fi
    
    docker-compose up -d || { echo "Failed to start Elasticsearch. Cleaning up..." ; cleanup; }
    cd ..
    echo "Elasticsearch setup complete."
}


function setup_kafka {
    echo "Setting up Kafka..."
    cd "../docker-compose/kafka/"
    
    docker-compose up -d || { echo "Failed to start Kafka. Cleaning up..." ; cleanup; }
    cd ..
    echo "Kafka setup complete."
}



function setup_postgresql {
    echo "Setting up postgresql..."
    cd "../docker-compose/postgreSQL/"
    docker-compose up -d || { echo "Failed to start Postgresql. Cleaning up..." ; cleanup; }
    until docker-compose exec -T pos3d-db psql -U postgres -c "SELECT 1" > /dev/null 2>&1; do
        sleep 1
    done

    # Create the databases
    docker-compose exec -T pos3d-db psql -U postgres -c "CREATE DATABASE \"product-svc\";"
    docker-compose exec -T pos3d-db psql -U postgres -c "CREATE DATABASE \"user-svc\";"
    docker-compose exec -T pos3d-db psql -U postgres -c "CREATE DATABASE \"order-svc\";"

    cd ..
    echo "Postgresql setup complete."
}


function setup_kong {
    echo "Setting up Kong..."
    cd "../docker-compose/kong/"
    
    # Check if the kong-net network exists
    if ! docker network inspect kong-net >/dev/null 2>&1; then
        docker network create kong-net
    else
        echo "kong-net network already exists."
    fi
    
    envFilePath=".env"
    pattern="KONG_PG_HOST=.*"
    replacement="KONG_PG_HOST=$(docker network inspect -f '{{range .IPAM.Config}}{{.Gateway}}{{end}}' kong-net)"
    sed -i "s|$pattern|$replacement|" "$envFilePath"
    
    docker-compose up -d || { echo "Failed to start Kong. Cleaning up..." ; cleanup; }
    cd ..
    echo "Kong setup complete."
}


function setup_kong_api {
    # Hàm kiểm tra tính khả dụng của URL
    function check_url_availability {
        response_code=$(curl -s -o /dev/null -w "%{http_code}" -X HEAD "$1/services")
        if [ "$response_code" -eq 200 ]; then
            return 0
        else
            return 1
        fi
    }
    
    # Kiểm tra tính khả dụng của URL
    while ! check_url_availability "http://localhost:8001"; do
        printf "Waiting for localhost:8001 to become online...%ss\r" "$((++seconds))"
        sleep 1
    done
    
    echo "Setting up Kong API..."
    cd "../script/kong-api/"
    echo "./config.sh -i $ip_address"
    ./config.sh -i $ip_address || { echo "Failed to configure Kong API. Cleaning up..." ; ./reset-kong.sh; }
    cd ..
    echo "Kong API setup complete."
}


# Địa chỉ IP của bạn
ip_address="ip_address"

while getopts ":i:" opt; do
    case $opt in
        i)
            ip_address=$OPTARG
        ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
        ;;
    esac
done

# Bỏ qua các tùy chọn đã xử lý
shift $((OPTIND -1))

# # Thực hiện setup Elasticsearch
setup_elasticsearch
setup_kafka

# # Thực hiện setup Kong
setup_kong
# # Thực hiện setup Kong API
setup_kong_api
# # Thực hiện setup DB
setup_postgresql

echo "Setup complete."
